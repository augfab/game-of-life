#include <stdlib.h>		// for srand, rand, RAND_MAX.
#include <unistd.h>		// for STDOUT_FILENO.
#include <assert.h>		// for assert.
#include <stdbool.h>		// for bool.
#include <term.h>		// for cur_term, setupterm, putp, tigetstr.

#include "life/utils.h"

static int rand_init_done = false;
static int rand_threshold = RAND_MAX;

void rand_init(unsigned seed, double threshold)
{
	srand(seed);
	rand_init_done = true;
	assert(threshold >= 0.0 && threshold <= 1.0);
	rand_threshold = (int)(threshold * RAND_MAX);
}

bool rand_bool()
{
	assert(rand_init_done);
	return rand() > rand_threshold;
}

void clrscr()
{
	if (!cur_term) {
		int result;
		setupterm(NULL, STDOUT_FILENO, &result);
		if (result <= 0)
			return;
	}
	putp(tigetstr("clear"));
}
