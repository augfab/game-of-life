#include <stdlib.h>		// for exit, EXIT_SUCCESS, abort.
#include <stdio.h>		// for sscanf.
#include <unistd.h>		// for sleep.
#include <stddef.h>		// for size_t.
#include <getopt.h>		// for getopt.

#include "life/utils.h"		// for rand_init, clrscr.
#include "life/life.h"		// for struct cells, c_*.

int main(int argc, char *argv[])
{
	size_t xsize = 10, ysize = 10;
	unsigned seed = 1;
	double threshold = 0.99;
	int cycles = -1;
	int display = 0;

	int c;
	while ((c = getopt(argc, argv, "s:t:x:y:c:d")) != -1) {
		switch (c) {
		case 's':
			sscanf(optarg, "%u", &seed);
			break;
		case 't':
			sscanf(optarg, "%lf", &threshold);
			break;
		case 'x':
			sscanf(optarg, "%zu", &xsize);
			break;
		case 'y':
			sscanf(optarg, "%zu", &ysize);
			break;
		case 'c':
			sscanf(optarg, "%d", &cycles);
			break;
		case 'd':
			display = 1;
			break;
		default:
			abort();
		}
	}

	rand_init(seed, threshold);

	struct cells *curr = c_create(xsize, ysize);

	c_randomize(curr);

	for (int i = 0; cycles == -1 || i < cycles; ++i) {
		if (display) {
			clrscr();
			c_print(curr);
			sleep(1);
		}
		c_next(curr);
	}
	c_delete(curr);
	exit(EXIT_SUCCESS);
}
