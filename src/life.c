#include <stdlib.h>		// for malloc, calloc, free, abort.
#include <stdio.h>		// for stdout, fputs, fflush.
#include <stdbool.h>		// for bool.
#include <stddef.h>		// for size_t.
#include <string.h>		// for memset.

#include "life/utils.h"		// for rand_bool.
#include "life/life.h"

#define LIVE '#'
#define DEAD '.'

struct cells {
	bool **cells;
	char **neigh;
	size_t xsize;
	size_t ysize;
};

struct cells *c_create(size_t const xsize, size_t const ysize)
{
	struct cells *c = malloc(sizeof(struct cells));
	if (c == NULL)
		abort();
	c->xsize = xsize;
	c->ysize = ysize;
	c->cells = calloc(xsize, sizeof(bool *));
	if (c->cells == NULL)
		abort();
	for (size_t x = 0; x < xsize; ++x) {
		c->cells[x] = calloc(ysize, sizeof(bool));
		if (c->cells[x] == NULL)
			abort();
	}
	c->neigh = calloc(xsize, sizeof(char *));
	if (c->neigh == NULL)
		abort();
	for (size_t x = 0; x < xsize; ++x) {
		c->neigh[x] = calloc(ysize, sizeof(bool));
		if (c->neigh[x] == NULL)
			abort();
	}
	return c;
}

void c_delete(struct cells *c)
{
	for (size_t x = 0; x < c->xsize; ++x) {
		free(c->neigh[x]);
		free(c->cells[x]);
	}
	free(c->neigh);
	free(c->cells);
	free(c);
}

void c_print(struct cells const *const c)
{
	for (size_t y = 0; y < c->ysize; ++y) {
		for (size_t x = 0; x < c->xsize; ++x) {
			fputc(c->cells[x][y] ? LIVE : DEAD, stdout);
		}
		fputs("\n", stdout);
	}
	fflush(stdout);
}

void c_randomize(struct cells *c)
{
	for (size_t x = 0; x < c->xsize; ++x)
		for (size_t y = 0; y < c->ysize; ++y)
			c->cells[x][y] = rand_bool();
}

void c_next(struct cells *c)
{
	for (size_t x = 0; x < c->xsize; ++x)
		memset(c->neigh[x], 0, c->ysize);

	for (size_t x = 0; x < c->xsize; ++x) {
		for (size_t y = 0; y < c->ysize; ++y) {
			if (c->cells[x][y]) {
				size_t const u = x == 0 ? c->xsize - 1 : x - 1;
				size_t const d = x == c->xsize - 1 ? 0 : x + 1;
				size_t const l = y == 0 ? c->ysize - 1 : y - 1;
				size_t const r = y == c->ysize - 1 ? 0 : y + 1;
				++c->neigh[x][l];
				++c->neigh[x][r];
				++c->neigh[u][y];
				++c->neigh[d][y];
				++c->neigh[u][l];
				++c->neigh[u][r];
				++c->neigh[d][l];
				++c->neigh[d][r];
			}
		}
	}

	for (size_t x = 0; x < c->xsize; ++x) {
		for (size_t y = 0; y < c->ysize; ++y) {
			switch (c->neigh[x][y]) {
			case 2:
				break;
			case 3:
				c->cells[x][y] = true;
				break;
			default:
				c->cells[x][y] = false;
				break;
			}
		}
	}
}
