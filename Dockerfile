FROM debian:stable

RUN apt-get -qq update && apt-get install --no-install-recommends -yqq \
	git make gcc libc-dev ncurses-dev \
	valgrind indent
