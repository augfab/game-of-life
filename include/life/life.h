#include <stddef.h>		// for size_t.

struct cells;

struct cells *c_create(size_t const, size_t const);
void c_delete(struct cells *);
void c_print(struct cells const *const);
void c_randomize(struct cells *);
void c_next(struct cells *);
