OBJS := $(addprefix build/, utils.o life.o main.o)

_CFLAGS := -Wall -Wextra -pedantic -ggdb -O2 -std=c11 $(CFLAGS)

COMPILE.c = $(CC) $(_CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
LINK.c = $(CC) $(_CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)

.PHONY: all clean

all: build/life

build:
	mkdir -p $@

build/life: $(OBJS) | build
	$(LINK.c) -o $@ $^ -lcurses

build/%.o : src/%.c | build
	$(COMPILE.c) -Iinclude -o $@ $<

clean:
	rm -rf build
