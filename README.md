# Conway's Game of Life
A toy implementation of the [famous game](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).

[![Pipeline](https://gitlab.com/augfab/game-of-life/badges/master/build.svg)](https://gitlab.com/augfab/game-of-life/pipelines?ref=master)

## Use
```
$ make
$ ./build/life -d -x 40 -y 30 -s "${RANDOM}" -t 0.8
```

## Profile
```
$ make -B CFLAGS='-g0 -pg'
$ ./build/life -x 1000 -y 1000 -c 1000
$ gprof ./build/life >results.stats
```

## Docker container
To build a new Docker container for CI:
```
$ docker build -t registry.gitlab.com/augfab/game-of-life .
$ docker push registry.gitlab.com/augfab/game-of-life
```
